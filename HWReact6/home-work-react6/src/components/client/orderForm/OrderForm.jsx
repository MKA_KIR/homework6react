import React from "react";
import {Formik, Field, Form, FieldArray} from "formik";
import FieldErrorMessage from '../../../shared/components/FieldErrorMessage'
import MaskedInput from 'react-text-mask';
import 'react-datepicker/dist/react-datepicker.css'
import DateView from 'react-datepicker';
import {fields} from './fields';
import {phoneNumberMask, emailMask} from "../../../shared/masks/";
import {initialValues} from "./initialValues";
import {orderFormSchema} from "./orderFormSchema";
import './OrderForm.css';

const OrderForm = () =>{
    const onSubmit = (values)=>{
        console.log(values);
    };

    const formProps = {
        initialValues,
        onSubmit,
        validationSchema: orderFormSchema
    }
    
const cityOptions = fields.city.options.map(({text, value}) => <option value = {value} >{text}</option>)
const deliveryOptions = fields.delivery.options.map(({text, value}) => <option value = {value} >{text}</option>)

    return(
        <Formik {...formProps}>
            <Form className='order-form'>
                <Field {...fields.firstName}/>
                <FieldErrorMessage name='firstName'/>
                <Field {...fields.secondName}/>
                <FieldErrorMessage name='secondName'/>
                <Field {...fields.lastname}/>
                <FieldErrorMessage name='lastname'/>
                <Field
                    {...fields.phone}
                    render={({ field }) => (
                        <MaskedInput
                            {...field}
                            mask={phoneNumberMask}
                            placeholder={fields.phone.placeholder}
                            className = 'form-control'
                        />
                    )}
                />
                <FieldErrorMessage name='phone'/>
                <Field
                    {...fields.email}
                    render={({ field }) => (
                        <MaskedInput
                            {...field}
                            mask={emailMask}
                            placeholder={fields.email.placeholder}
                            className = 'form-control'
                        />
                    )}
                />
                <FieldErrorMessage name='email'/>
                <Field as='select' {...fields.city}>{cityOptions}</Field>
                <Field as='select' {...fields.delivery}>{deliveryOptions}</Field>
                <FieldArray>
                    {({form}) =>{
                        if(form.values.delivery === 'New mail'){
                            return(
                                <>
                                <Field {...fields.departmentNumber}/>
                                <FieldErrorMessage name='departmentNumber'/>
                                </>
                            )
                        }
                        if(form.values.delivery === 'Express delivery'){
                            return (
                                <>
                                <Field {...fields.street}/>
                            <FieldErrorMessage name='street'/>
                            <Field {...fields.house}/>
                            <FieldErrorMessage name='house'/>
                            <Field {...fields.apartment}/>
                            <FieldErrorMessage name='apartment'/>

                            <Field {...fields.deliveryTime}>
                            {({ form, field }) => {
                            const { setFieldValue } = form
                                    const { value } = field
                                    return (
                                        <DateView
                                            {...field}
                                            className = 'form-control'
                                            placeholderText= 'deliveryTime'
                                            selected={value}
                                            onChange={val => setFieldValue(fields.deliveryTime.name, val)}
                                        />
                                    )
                                }}
                            </Field>
                            <FieldErrorMessage name='deliveryTime'/>
                            </>
                            )
                        }
                    }}
                </FieldArray>

                <div>
                    <Field  {...fields.checkbox}/>
                        <label htmlFor={fields.checkbox.id}>{fields.checkbox.label}</label>
                    <FieldErrorMessage name='checkbox'/>
                </div>
                <Field as='textarea' {...fields.comments}/>
                <Field {...fields.submit}/>
            </Form>
        </Formik>
    )
}
export default OrderForm;