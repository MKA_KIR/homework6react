import * as Yup from "yup";
import validationsMessage from "../../../shared/validation";

export const orderFormSchema = Yup.object().shape({
    firstName: Yup.string().required(validationsMessage.required).min(2, validationsMessage.firstName),
    secondName: Yup.string().required(validationsMessage.required).min(2, validationsMessage.secondName),
    lastname: Yup.string().required(validationsMessage.required).min(2, validationsMessage.lastname),
    phone: Yup.string().required(validationsMessage.required).min(2, validationsMessage.phone),
    email: Yup.string().required(validationsMessage.required).email(validationsMessage.email),
    departmentNumber: Yup.string().required(validationsMessage.required).min(1, validationsMessage.departmentNumber),
    street: Yup.string().required(validationsMessage.required).min(2,validationsMessage.street),
    house: Yup.string().required(validationsMessage.required).min(1, validationsMessage.house),
    apartment: Yup.string().required(validationsMessage.required).min(1, validationsMessage.apartment),
    deliveryTime: Yup.string().required(validationsMessage.required).min(2,validationsMessage.deliveryTime),
    checkbox: Yup.bool()
});