export const initialValues = {
    firstName: "",
    secondName: "",
    lastname: "",
    phone:"",
    email: "",
    departmentNumber:"",
    street:"",
    house:"",
    apartment:"",
    deliveryTime:"",
    city:'Kiev',
    delivery:'New mail',
    checkbox: false,
    submit: "Order"
};