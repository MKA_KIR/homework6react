export const fields = {
    firstName:{
        name:'firstName',
        placeholder: 'firstName',
        className: 'form-control'
    },
    secondName:{
        name:'secondName',
        placeholder: 'secondName',
        className: 'form-control'
    },
    lastname:{
        name:'lastname',
        placeholder: 'lastname',
        className: 'form-control'
    },
    phone:{
        name:'phone',
        placeholder: 'phone',
        className: 'form-control'
    },
    email: {
        name: "email",
        placeholder: "email",
        className: 'form-control'
    },
    city:{
        name:'city',
        options:[
            {
                value:'Kiev',
                text:'Киев'
            },
            {
                value:'Kharkov',
                text:'Харьков'
            },
            {
                value:'Dnipro',
                text:'Днепр'
            },
            {
                value:'Odessa',
                text:'Одесса'
            },
            {
                value:'Lviv',
                text:'Львов'
            }
        ]
    },
    delivery:{
        name:'delivery',
        options:[
            {
                value:'New mail',
                text:'Новая почта'
            },
            {
                value:'Express delivery',
                text:'Курьерская доставка'
            }
        ]
    },
    departmentNumber:{
        name:'departmentNumber',
        placeholder:'departmentNumber',
        className: 'form-control'
    },

    street:{
        name:'street',
        placeholder:'street',
        className: 'form-control'
    },
    house :{
        name:'house',
        placeholder:'house',
        className: 'form-control'
    },
    apartment:{
        name:'apartment',
        placeholder:'apartment',
        className: 'form-control'
    },
    deliveryTime :{
        name:'deliveryTime',
        placeholder:'deliveryTime',
    },
    checkbox:{
        name:'checkbox',
        label:'подписаться на рассылку акций',
        type:'checkbox',
        id: 'subscribe-newsletter',
    },
    comments:{
        name:'comments',
        placeholder:'comments',
        className: 'form-control'

    },
    submit: {
        name: "submit",
        type: "submit"
    }
}