import React from 'react'
import {ErrorMessage} from 'formik'
import './FieldErrorMessage.css'

const FieldErrorMessage = ({name}) =>{
    return(
        <ErrorMessage name={name} >
            {(errorMessage) => {
            return <p className="field-error">{errorMessage}</p>}}
        </ErrorMessage>
    )
}
export default FieldErrorMessage